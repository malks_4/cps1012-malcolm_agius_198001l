#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include "functions.h"
#include "environment.h"
#include "linenoise.h"

#define MAX_ARGS 255

static bool is_source = false;
static bool background = false;


static void sigtstp_handler(int signum){
    char msg[] = "SIGTSTP caught";
    write(STDOUT_FILENO,msg,sizeof(msg));
}

static void sigint_handler(int signum){
    char msg[] = "SIGINT caught";
    write(STDOUT_FILENO,msg,sizeof(msg));
}

static void install_handler(int signum, sig_t handler) {
    if (signal(signum, handler) == SIG_ERR) {
        psignal(signum, "Cannot set handler");
        exit(EXIT_FAILURE);
    }
}

int main() {
    listproc_count = 0;
    const int stdin_holder = dup(0);
    const int stdout_holder = dup(1);
    char *line,*token = NULL,
    *args[MAX_ARGS];
    int tokenIndex;

    if(set_env_vars()==false){  //setting up env
        perror("Error setting environment variables\n");
        return EXIT_FAILURE;
    }

    install_handler(SIGTSTP, &sigtstp_handler);
    install_handler(SIGINT, &sigint_handler);
    while (true){
        int no_of_pipes = 0;
        if(is_source==false){
            char* prompt = getenv("PROMPT");    //error handling for prompt
            if(prompt==NULL){
                perror("couldn't get prompt\n");
                return EXIT_FAILURE;
            }
            line = linenoise(prompt);
            if((line!=NULL)&&(strcmp(line,"\0")!=0)){   //to handle if user just presses enter
                tokenize(args,&tokenIndex,token,line,&no_of_pipes,&background);  //splits line into args
                if(execute(args,&tokenIndex,&is_source,&no_of_pipes,&background)==true){
                    for(int j = 0;j<listproc_count;j++){
                        free(listProc[j].name);
                    }
                    exit(EXIT_SUCCESS);
                }
                //displays error messages
                // Free allocated memory
                if(is_source==false){
                    linenoiseFree(line);
                    int j = 0;
                    while(args[j]!=NULL){   //flushes args
                        args[j] = NULL;
                        j++;
                    }
                }
            }
        }
        else{
            tokenize(args,&tokenIndex,token,line,&no_of_pipes,&background);
            int ret = source_c(args,&tokenIndex,token,line,&is_source,&no_of_pipes,&background);
            if(ret==false){
                perror("source command failed\n");
            }
            else if(ret==SOURCE_EXIT){
                for(int j = 0;j<listproc_count;j++){    //free allocated memory
                    free(listProc[j].name);
                }
                exit(EXIT_SUCCESS);
            }
            is_source = false;
        }
        dup2(stdin_holder,0);   //resetting to standard I/O
        dup2(stdout_holder,1);
        background = false;
    }
    return EXIT_SUCCESS;
}
