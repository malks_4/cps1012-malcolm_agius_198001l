//
// Created by malks4 on 5/18/20.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <ctype.h>
#include <signal.h>
#include <limits.h>
#include "functions.h"
#include "environment.h"


#define MAX_ARGS 255

void tokenize(char* args[],int* tokenIndex,char* token,char* line,int* no_of_pipes,bool* background){
    token = strtok(line, " ");
    //tokenizes user input

    for (*tokenIndex = 0;token != NULL && *tokenIndex < MAX_ARGS - 1;*tokenIndex+=1) {
        args[*tokenIndex] = token;
        if(strcmp(args[*tokenIndex],"|")==0){
            *no_of_pipes+=1;
        }
        token = strtok(NULL, " ");
    }
    if(strcmp(args[*tokenIndex-1],"&")==0){     //checks for &
        args[*tokenIndex-1] = NULL;
        *tokenIndex-=1;
        *background = true;
    }
}

bool parse(char* args[],int *tokenIndex){
    char* prefix = NULL;
    char* env_name = NULL;
    char* suffix = NULL;
    for(int i = 0;i<*tokenIndex;i++){    //loop through all args
        if((strpbrk(args[i],"$")!=NULL)&&(args[i][0]!='\\')){     //checks if arg includes $ character
            if(args[i][0]!='$'){    //if first char of args[i] != $
                prefix = strtok(args[i],"$");   //gets string before $
                env_name = args[i]+(strlen(prefix)+1); //gets string after $
            }
            else{
                env_name = args[i]+1;   //gets env_name without dollar
            }
            int j = 0;
            int punctIndex = -1;
            while(env_name[j]){     //finds index of first punct of env_name(to get suffix)
                if((!isalpha(env_name[j]))&&(!isdigit(env_name[j]))&&(env_name[j]!='_')){
                    punctIndex = j;
                    break;
                }
                j++;
            }
            char* suffix_holder = NULL;
            if(punctIndex!=-1){
                suffix = env_name+punctIndex;
                suffix_holder = malloc(sizeof(char)*strlen(suffix));    //holds suffix because suffix pointer is nulled in the next line is nulled
                strcpy(suffix_holder,suffix);
                env_name[punctIndex] = '\0';    //to remove suffix
            }
            char* env_var = getenv(env_name);
            int arg_length = 0;
            if(prefix!=NULL)
                arg_length+=(int)strlen(prefix);
            if(env_var!=NULL)
                arg_length+=(int)strlen(env_var);
            if(suffix_holder!=NULL)
                arg_length+=(int)strlen(suffix_holder);
            char* ans = (char*)malloc(sizeof(char)*arg_length);       //concatenating strings
            if(prefix!=NULL){
                strcpy(ans,prefix);
                if(env_var!=NULL)
                    strcat(ans,env_var);
                else
                    strcat(ans,"(null)");
                if(suffix_holder!=NULL)
                    strcat(ans,suffix_holder);
            }
            else{
                if(env_var!=NULL)
                    strcpy(ans,env_var);
                else
                    strcpy(ans,"(null)");
                if(suffix_holder!=NULL)
                    strcat(ans,suffix_holder);
            }
            strcpy(args[i],ans);
            free(suffix_holder);
            free(ans);
        }
        else if(args[i][0]=='\\'){
            args[i] = args[i]+1;
        }
        char* quote_ptr_open;
        if((quote_ptr_open=strpbrk(args[i],"\""))!=NULL){    //concatenating arguments within;
            if(strpbrk(quote_ptr_open+1,"\"")==NULL){    //checks if arg contains "
                int count = 0;      //counts no of args between " "
                bool closed = false;    //true if arg closes ""
                do{
                    count++;
                    char* next = (char*)malloc(sizeof(strlen(args[i+count]))); //holds next string to be concatenated
                    strcpy(next,args[i+count]);
                    strcat(args[i]," ");
                    if(strpbrk(next,"\"")==NULL){   //checks if arg contains "
                        strcat(args[i],next);
                    }
                    else{                                       //this code concatenates args between ""
                        strcat(args[i],next);
                        closed = true;
                    }
                    free(next);
                }while((!closed)&&((count+i)<*tokenIndex-1));     //loop if args not closed or all args checked
                if(!closed){
                    return false;   //error handling for quotes not closed
                }
                for(int j = i+1;j<(*tokenIndex-count);j++){         //shifts remaining args to the left
                    args[j]=args[j+count];
                }
                char *src, *dest;       //code to remove " from arg taken from https://stackoverflow.com/questions/4161822/remove-all-occurences-of-a-character-in-c-string-example-needed
                src = dest = args[i];    // both pointers point to the first char of input
                while(*src != '\0')    // exit loop when null terminator reached
                {
                    if (*src != '\"')  // if source is not a " char
                    {
                        *dest = *src;  // copy the char at source to destination
                        dest++;        // increment destination pointer
                    }
                    src++;             // increment source pointer
                }
                *dest = '\0';          // terminate string with null terminator

                *tokenIndex-=count;     //rearranges counter of args
            }
        }
    }
    return true;
}

int execute(char* args[],int* tokenIndex,bool* is_source,int* no_of_pipes,bool* background){   //when this function returns true, the user wants to exit
    int error;
    error=redirect(args,tokenIndex,is_source,no_of_pipes,background);   //checks for redirects
    if(error==false){   //false indicates input redirection
        return false;   //thus end this function and this function will be called again with new args
    }
    int commandIndex = -1;
    if(error==true){
        if(*no_of_pipes>0){
            error = piping(args,tokenIndex,no_of_pipes,background);
        }
        else{
            for(int i = 0;i<7;i++){     //checks if the command is internal
                if(strcmp(args[0],command[i])==0){
                    commandIndex = i;
                    break;
                }
            }
            if((strpbrk(args[0],"=")!=NULL)&&(commandIndex==-1)){
                commandIndex = containequals;
            }
            if (strcmp(args[0], "exit") == 0){   //exit command
                return true;
            }
            // set last token to NULL
            args[*tokenIndex] = NULL;
            switch(commandIndex){   //all internal commands except for exit implemented above
                case echo:
                    error = echo_c(args,tokenIndex);
                    break;
                case cd:
                    if((error=cd_c(args,tokenIndex))!=true){
                        fprintf(stderr,"cd command failed\n");
                    }
                    break;
                case unset:
                    if((error=unset_c(args,tokenIndex))!=true){
                        fprintf(stderr,"unset command failed\n");
                    }
                    break;
                case showenv:
                    if((error=showenv_c(args,tokenIndex))!=true){
                        fprintf(stderr,"showenv command failed\n");
                    }
                    break;
                case source:
                    *is_source=true;
                    break;
                case listproc:
                    if((error=listproc_c(tokenIndex))!=true){
                        fprintf(stderr,"listproc command failed\n");
                    }
                    break;
                case bg:
                    if((error=bg_c(args,tokenIndex))!=true){
                        fprintf(stderr,"bg command failed\n");
                    }
                case containequals:
                    if((error=containequals_c(args,tokenIndex))!=true){
                        fprintf(stderr,"setting environment variable failed\n");
                    }
                    break;
                default:
                    commandIndex=-1;
                    if((error=fork_exec(args,tokenIndex,background))==false){
                        fprintf(stderr,"fork exec failed\n");
                    }
                    break;
            }
        }
    }

    if(commandIndex!=-1)        //to exclude error handler on fork_exec which does its own error handling
        error_handler(&error);
    return false;
}


int fork_exec(char* args[],int* tokenIndex,bool* background){
    if(parse(args,tokenIndex)==false){  //parse strings
        return ERRQUOTE;
    }
    char* args_temp[*tokenIndex+1];
    for(int i = 0;i<*tokenIndex;i++){
        args_temp[i] = (char*)malloc(sizeof(strlen(args[i])));
        strcpy(args_temp[i],args[i]);
        args_temp[i][strlen(args[i])]='\0';
    }
    args_temp[*tokenIndex] = NULL;
    // Basic error checking
    if (args == NULL || *args == NULL)
        return false;

    // Fork and exec
    pid_t pid_result = fork();
    if (pid_result == -1) {
        return ERRFORK;
    }
    listProc[listproc_count].pid = pid_result;  //storing pid of forked process for later use
    listProc[listproc_count].name = (char*)(malloc(strlen(args[0])));
    strcpy(listProc[listproc_count].name,args[0]);
    listproc_count++;
    if (pid_result == 0) {
        // This is child - call search_exec_path
        if(*background){
            if(setpgid(0,0)==-1){       //changes process group of this process thus output not shown
                perror("Error setting PGID\n");
                return EXIT_FAILURE;
            }
        }
        int ret = search_exec_path(args_temp);   //we use temp args in case of args with ""
        if(ret!=true){
            if(ret==CUSTOM_EXIT_CODE){
                exit(CUSTOM_EXIT_CODE);
            }
            else{
                return ret;
            }
        }
    } else {
        // This is parent
        for(int i = 0;i<*tokenIndex;i++){
            free(args_temp[i]);
        }
        if(!*background){
            int status;
            pid_t wait = waitpid(pid_result,&status,WUNTRACED | WCONTINUED);
            if (wait == -1) {
                perror("waitpid failed");
                return ERRWAIT;
            }

            // search_exec_path failed (using custom error code 4)
            if (WIFEXITED(status) && WEXITSTATUS(status) == CUSTOM_EXIT_CODE) {
                return CUSTOM_EXIT_CODE;
            }
            else if(WIFSTOPPED(status)){
                printf("\n");
                return true;
            }
        }
        else{
            printf("Process [%s] with PID [%d] running in the background\n",args[0],pid_result);
        }
    }
    return true;
}

int search_exec_path(char* args[]){
    char* path = getenv("PATH");    //getting path
    if(path==NULL){
        perror("error getting path\n");
        return ERRGET;      //error handling
    }
    int no_of_paths = 1;    //starts at 1 because no_of_path is always no os ':'s +1
    for(int i = 0;path[i]!='\0';i++){
        if(path[i]==':'){
            no_of_paths++;
        }
    }
    char* paths_div[no_of_paths];
    char* token = strtok(path,":");     //tokenize
    int pathIndex = 0;
    while (token!=NULL) {               //tokenizing
        paths_div[pathIndex] = (char*)malloc(sizeof(char)*strlen(token));
        strcpy(paths_div[pathIndex],token);
        token = strtok(NULL, ":");
        pathIndex++;
    }
    bool accessed = false;
    for(int i = 0;i<no_of_paths;i++){
        strcat(paths_div[i],"/");       //concatenate /args[0] to every path_div
        strcat(paths_div[i],args[0]);
        if(access(paths_div[i],X_OK)==0){
            accessed = true;
            if(execv(paths_div[i],args)==-1){
                perror("execv failed");    //execute command
                for(int j = 0;j<no_of_paths;j++){   //free allocated memory
                    free(paths_div[j]);
                }
                return CUSTOM_EXIT_CODE;
            }
            break;
        }
    }
    for(int i = 0;i<no_of_paths;i++){   //free allocated memory
        free(paths_div[i]);
    }
    if(!accessed){
        perror("Command not found\n");
        return CUSTOM_EXIT_CODE;
    }

    return true;
}

int echo_c(char* args[],int* tokenIndex){
    if(parse(args,tokenIndex)==false){
        return ERRQUOTE;
    }
    for(int i = 1;i<*tokenIndex;i++){    //loop through all args
        printf("%s ",args[i]);
    }
    printf("\n");
    return true;
}

int cd_c(char* args[],int* tokenIndex){
    if(parse(args,tokenIndex)==false){  //parse strings
        return ERRQUOTE;
    }
    if(*tokenIndex>2){
        return ERR_MANY_ARGS;
    }
    bool success = true;
    char* cwd = getenv("CWD");
    char* cwd_temp = (char*)(malloc(strlen(cwd)+strlen(args[1])));
    strcpy(cwd_temp,cwd);
    if(chdir(args[1])==-1){     //checks for whole path in arg
        success = false;
        if(cwd==NULL){
            return ERRGET;
        }
        strcat(cwd_temp,args[1]);
        if(chdir(cwd_temp)==-1){
            return ERRCD;
        }
    }
    else{
        char* new_cwd = (char*)(malloc(strlen(args[1])));
        strcpy(new_cwd,args[1]);
        if(setenv("CWD",new_cwd,1)==-1){
            free(new_cwd);
            return ERRSET;
        }
        free(new_cwd);
    }
    if((strcmp(args[1],"..")!=0)&&(!success)){
        if(setenv("CWD",cwd_temp,1)==-1){
            return ERRSET;
        }
    }
    else if(strcmp(args[1],"..")==0){
        if(cwd==NULL){
            return ERRGET;
        }
        for(int i = (int)strlen(cwd)-1;i>=0;i--){
            if(cwd[i]=='/'){
                cwd[i]='\0';
                if(setenv("CWD",cwd,1)==-1){
                    return ERRSET;
                }
                break;
            }
        }
    }
    free(cwd_temp);
    return true;
}

int unset_c(char* args[],int* tokenIndex){
    if(*tokenIndex>2){
        return ERR_MANY_ARGS;
    }
    if(*tokenIndex<2){
        return ERR_FEW_ARGS;
    }
    if(unsetenv(args[1])==-1){
        return ERRUNSET;
    }
    return true;
}

int showenv_c(char* args[],int* tokenIndex){
    if(*tokenIndex>1){
        return ERR_MANY_ARGS;
    }
    int i = 0;
    while(environ[i]!=NULL){
        printf("%s\n",environ[i]);
        i++;
    }
    return true;
}

int source_c(char* args[],int* tokenIndex,char* token,char* line,bool* is_source,int* no_of_pipes,bool* background){ //this function reads the commands from a text file and saves them into linked list
    int arg_count = 0;
    while((args[arg_count]!=NULL)){  //counting args
        arg_count++;
    }
    if(arg_count>2){
        return ERR_MANY_ARGS;
    }
    if(arg_count<2){
        return ERR_FEW_ARGS;
    }
    FILE *fp;
    if((fp = fopen(args[1],"r"))==NULL){
        return ERR_READ_FILE;
    }
    while((fgets(line,PATH_MAX,fp))!=NULL){
        if(line[strlen(line)-1]=='\n'){
            line[strlen(line)-1] = '\0';
        }
        tokenize(args,tokenIndex,token,line,no_of_pipes,background);
        if((execute(args,tokenIndex,is_source,no_of_pipes,background)==true)){   //exit command handler
            return SOURCE_EXIT;
        }
    }
    return true;
}

int listproc_c(int* tokenIndex){
    if(*tokenIndex>1){
        return ERR_MANY_ARGS;
    }
    printf("PID\tCMD\n");
    bool running_process = false;
    for(int i = 0;i<listproc_count;i++){
        int status;
        pid_t check = waitpid(listProc[i].pid,&status,WNOHANG);
        if(check==0){
            running_process = true;
            printf("%d\t%s\n",listProc[i].pid,listProc[i].name);
        }
    }
    if(!running_process){
        printf("None\tNone");
    }
    return true;
}

int bg_c(char* args[],int* tokenIndex){
    if(*tokenIndex>2){
        return ERR_MANY_ARGS;
    }
    if(*tokenIndex<2){
        return ERR_FEW_ARGS;
    }
    pid_t pid = atoi(args[1]);
    //sscanf(args[1],"%d",&pid);
    if(kill(SIGCONT,pid)==-1){
        perror("SIGCONT failed");
        return false;
    }
    return true;
}
