//
// Created by malks4 on 5/18/20.
//

#include <stdbool.h>

#ifndef ASSIGNMENT_FUNCTIONS_H
#define ASSIGNMENT_FUNCTIONS_H
#define LISTPROC_MAX 100

enum command{echo,cd,unset,showenv,source,listproc,bg,containequals}; //enum to represent
static const char* command[] = {"echo","cd","unset","showenv","source","listproc","bg"};

struct process{
    pid_t pid;
    char* name;
};

struct process listProc[LISTPROC_MAX];
int listproc_count;

void tokenize(char* args[],int* tokenIndex,char* token,char* line,int* no_of_pipes,bool* background);
bool parse(char* args[],int* tokenIndex);
int execute(char* args[],int* tokenIndex,bool* is_source,int* no_of_pipes,bool* background);
int fork_exec(char* args[],int* tokenIndex,bool* background);
int search_exec_path(char* args[]);
int echo_c(char* args[],int* tokenIndex);
int cd_c(char* args[], int* tokenIndex);
int unset_c(char* args[],int* tokenIndex);
int showenv_c(char* args[],int* tokenIndex);
int source_c(char* args[],int* tokenIndex,char* token,char* line,bool* is_source,int* no_of_pipes,bool* background);
int listproc_c(int* tokenIndex);
int bg_c(char* args[],int* tokenIndex);


#endif //ASSIGNMENT_FUNCTIONS_H
