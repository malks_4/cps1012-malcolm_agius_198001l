//
// Created by malks4 on 5/20/20.
//

#ifndef ASSIGNMENT_ENVIRONMENT_H
#define ASSIGNMENT_ENVIRONMENT_H

#include "functions.h"

#define SOURCE_EXIT 2
#define ERRSET 3
#define CUSTOM_EXIT_CODE 4
#define ERRFORK 5
#define ERRWAIT 6
#define ERR_MANY_ARGS 7
#define ERRCD 8
#define ERRGET 9
#define ERRUNSET 10
#define ERR_FEW_ARGS 11
#define ERR_READ_FILE 12
#define ERREQUALS 13
#define ERRPATH 14
#define ERRQUOTE 15
#define ERRPIPE 16


extern char** environ;

typedef void(*sig_t)(int);

int set_env_vars();
int containequals_c(char* args[], int* tokenIndex);
int reopen(int fd,const char* pathname,int flags,mode_t mode);
int redirect_input(const char *input);
int redirect_output_ow(const char* output);
int redirect_output(const char* output);
bool redirect(char* args[],int* tokenIndex,bool* is_source,int* no_of_pipes,bool* background);
void error_handler(const int* error);
int piping(char* args[],int* tokenIndex,const int* no_of_pipes,bool* background);

#endif //ASSIGNMENT_ENVIRONMENT_H
