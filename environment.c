//
// Created by malks4 on 5/20/20.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <ctype.h>
#include <signal.h>
#include <limits.h>
#include <fcntl.h>
#include "environment.h"
#include "functions.h"
#include "linenoise.h"

#define MAX_ARGS 255


enum envvar{PATH,PROMPT,CWD,USER,HOME,SHELL,TERMINAL,EXITCODE}; //enums for different envvars unique to eggshell

int set_env_vars(){
    char envvar[8][PATH_MAX];   //holds variables unique to eggshell (temporarily)
    char* path = getenv("PATH");    //getting PATH (to be used for SHELL)
    if(path==NULL){
        printf("Unable to retrieve path\n");
        return ERRSET;
    }
    strcpy(envvar[PROMPT],"€> ");
    if(setenv("PROMPT",envvar[PROMPT],1)==-1){  //setting PROMPT
        return ERRSET;
    }
    if(getcwd(envvar[CWD],PATH_MAX)==NULL){     //getting CWD using getcwd function
        return ERRSET;
    }
    if(setenv("CWD",envvar[CWD],1)==-1){    //setting CWD
        return ERRSET;
    }
    strcpy(envvar[SHELL],envvar[CWD]);
    strcat(envvar[SHELL],"/main");                      //producing SHELL
    if(setenv("SHELL",envvar[SHELL],1)==-1){    //setting SHELL
        return ERRSET;
    }
    char* tty = ttyname(STDOUT_FILENO);     //getting TERMINAL
    if(tty==NULL){
        return ERRSET;
    }
    strcpy(envvar[TERMINAL],tty);
    if(setenv("TERMINAL",envvar[TERMINAL],1)==-1){    //setting TERMINAL
        return ERRSET;
    }
    if(setenv("EXITCODE","0",1)==-1){
        return ERRSET;
    }
    return true;
}

void error_handler(const int* error){   //prints appropriate error message and sets
    if(*error!=true){
        switch(*error){
            case ERRSET:
                fprintf(stderr,"Error setting environment variable\n");
                break;
            case ERRFORK:
                fprintf(stderr,"Unable to fork\n");
                break;
            case ERRWAIT:
                fprintf(stderr,"Unable to wait\n");
                break;
            case ERR_MANY_ARGS:
                fprintf(stderr,"Too many arguments\n");
                break;
            case ERR_FEW_ARGS:
                fprintf(stderr,"Too few arguments\n");
                break;
            case ERRCD:
                fprintf(stderr,"Unable to change directory\n");
                break;
            case ERRGET:
                fprintf(stderr,"Unable to get environment variable\n");
                break;
            case ERRUNSET:
                fprintf(stderr,"Unable to unset environment variable\n");
                break;
            case ERR_READ_FILE:
                fprintf(stderr,"Unable to read from file\n");
                break;
            case ERREQUALS:
                fprintf(stderr,"Cannot set environment variable without specifying variable name\n");
                break;
            case ERRQUOTE:
                fprintf(stderr,"Quote not closed\n");
                break;
            case ERRPIPE:
                fprintf(stderr,"Unable to set up pipe\n");
            default:
                fprintf(stderr,"Error\n");
                break;
        }
    }
    char err[3];
    sprintf(err,"%d",*error);
    if(setenv("EXITCODE",err,1) == -1){
        perror("Unable to set EXITCODE");
    }
}

int containequals_c(char* args[], int* tokenIndex){
    parse(args,tokenIndex);
    if(*tokenIndex>1){
        return ERR_MANY_ARGS;
    }
    if(args[0][0]=='='){
        return ERREQUALS;
    }
    char* new_env;
    new_env = strtok(args[0], "=");     //gets env var name
    int new_env_length = (int)strlen(new_env);
    if(setenv(new_env,args[0]+new_env_length+1,1)==-1){
        return ERRSET;  //setenv exit code
    }
    return true;
}

int reopen(int fd,const char* pathname,int flags,mode_t mode){
    int open_fd = open(pathname,flags,mode);
    if(open_fd == fd || open_fd < 0){
        return open_fd;
    }
    int dup_fd = dup2(open_fd, fd);
    close(open_fd);

    return dup_fd == -1 ? dup_fd : fd;
}

int redirect_input(const char *input){
    return (reopen(STDIN_FILENO, input, O_RDONLY, S_IRUSR));
}

int redirect_output_ow(const char* output){
    return (reopen(STDOUT_FILENO,output, O_RDWR | O_CREAT | O_TRUNC,
            S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH | S_IWOTH));
}

int redirect_output(const char* output){
    return (reopen(STDOUT_FILENO,output, O_RDWR | O_CREAT | O_APPEND,
                   S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH | S_IWOTH));
}

bool redirect(char* args[],int* tokenIndex,bool* is_source,int* no_of_pipes,bool* background){    //this method check for redirection
    short type = -1;
    int position;
    for(int i = 0;i<*tokenIndex;i++){
        if(strcmp(args[i],">")==0){
            type = 0;
            position = i;
            break;
        }
        else if(strcmp(args[i],">>")==0){
            type = 1;
            position = i;
            break;
        }
        else if(strcmp(args[i],"<")==0){
            type = 2;
            position = i;
            break;
        }
    }
    char* new_args[MAX_ARGS];   //includes command entered by keyboard and args gotten from file
    char* post_args[MAX_ARGS];  //includes arguments after the input redirects
    //*tokenIndex-=2;
    if(type!=-1){
        switch(type){
            case 0:
                *tokenIndex-=2;
                redirect_output_ow(args[position+1]);
                break;
            case 1:
                *tokenIndex-=2;
                redirect_output(args[position+1]);
                break;
            case 2:
                for(int i = 0;i<position;i++){
                    new_args[i] = (char*)(malloc(strlen(args[i])));
                    strcpy(new_args[i],args[i]);
                }
                int count = 0;
                for(int i = position+2;i<*tokenIndex;i++){  //copying args after redirect into an array
                    post_args[i-position-2] = (char*)(malloc(strlen(args[i])));
                    strcpy(post_args[i-position-2],args[i]);
                    count++;
                }
                redirect_input(args[position+1]);
                char* line = linenoise(""); //takes in input from file
                char* token = NULL;
                char* args_from_file[MAX_ARGS];
                tokenize(args_from_file,tokenIndex,token,line,no_of_pipes,background); //tokenizes args from file
                int count_file = 0;
                for(int i = 0;i<*tokenIndex;i++){
                    new_args[i+position] = (char*)(malloc(strlen(args[i])));    //adding args from file to previous args
                    strcpy(new_args[i+position],args_from_file[i]);
                    count_file++;
                }
                for(int i = 0;i<count;i++){
                    new_args[position+count_file+i] = (char*)(malloc(strlen(post_args[i])));   //adding args after the args from file to the rest
                    strcpy(new_args[position+count_file+i],post_args[i]);
                }
                *tokenIndex+=(position+count);  //recalibrate tokenIndex
                parse(new_args,tokenIndex);     //parse new args
                execute(new_args,tokenIndex,is_source,no_of_pipes,background); //execute updated args
                for(int i = 0;i<*tokenIndex;i++){
                    free(new_args[i]);      //free memory
                }
                linenoiseFree(line);        //free linenoise
                return false; //this signifies that the input has been redirected
        }
    }
    return true;
}

int piping(char* args[],int* tokenIndex,const int* no_of_pipes,bool* background){
    int arg_count = 0;
    int no_of_children = *no_of_pipes+1;
    int fd[*no_of_pipes * 2],
    *current_fd = fd,
    *previous_fd = NULL;

    for(int stage = 0;stage<no_of_children;stage++){
        previous_fd = current_fd-2; //invaluable for first stage
        char* pipe_args[MAX_ARGS];
        int this_stage_count = 0;
        for(int i = 0;(arg_count<*tokenIndex)&&(strcmp(args[arg_count],"|")!=0);arg_count++,i++){    //loop till end of args or current arg = "|"
            pipe_args[i] = (char*)(malloc(strlen(args[arg_count])));
            strcpy(pipe_args[i],args[arg_count]);
            this_stage_count++;
        }

        pipe_args[this_stage_count] = NULL;    //NULL terminated array for execv
        arg_count++;    //to skip "|"

        if(stage < no_of_children-1){
            if(pipe(current_fd)==-1){
                return ERRPIPE;         //returns error
            }
        }

        pid_t pid_result = fork();
        if(pid_result==-1){
            return ERRFORK;
        }

        else if(pid_result==0){ //child
            if(stage<no_of_children-1){     //for all stages but the last
               close(current_fd[0]);
               dup2(current_fd[1],STDOUT_FILENO);   //sets the output of pipe
               close(current_fd[1]);
            }

            if(stage>0){                        //set up input of pipe if pipe is not first pipe
                close(previous_fd[1]);
                dup2(previous_fd[0],STDIN_FILENO);
                close(previous_fd[0]);
            }

            if(*background){
                if(setpgid(0,0)==-1){       //changes process group of this process thus output not shown
                    perror("Error setting PGID\n");
                    return EXIT_FAILURE;
                }
            }

            int ret = search_exec_path(pipe_args);   //execute command
            if(ret==CUSTOM_EXIT_CODE){
                exit(CUSTOM_EXIT_CODE);
            }
            else{
                return ret;
            }
        }
        if(stage>=1){
            close(previous_fd[0]);  //close file descriptors in parent
            close(previous_fd[1]);
        }
        current_fd+=2;
        for(int i = 0;i<this_stage_count;i++){   //free allocated memory
            free(pipe_args[i]);
        }
        if(!*background){
            int status;
            pid_t wait = waitpid(pid_result, &status, WUNTRACED | WCONTINUED);
            if (wait == -1) {
                perror("waitpid failed");
                return ERRWAIT;
            }

            if (WIFEXITED(status) && WEXITSTATUS(status) == CUSTOM_EXIT_CODE) {
                return CUSTOM_EXIT_CODE;
            }
            else if(WIFSTOPPED(status)){
                return true;
            }
        }
    }
    return true;
}


